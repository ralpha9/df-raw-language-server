use jsonrpc_ipc_server::jsonrpc_core::*;
use jsonrpc_ipc_server::ServerBuilder;

fn main() {
    println!("Hello, world!");
    // start_language_server();
    df_ls_syntax::parse();
    println!("Done!");
}

#[allow(dead_code)]
fn start_language_server() {
    let mut io = IoHandler::new();
    io.add_method("say_hello", |_params| Ok(Value::String("hello".into())));

    let builder = ServerBuilder::new(io);
    let server = builder
        .start("/tmp/df-raw-lsp-ipc.ipc")
        .expect("Couldn't open socket");
    server.wait();
}
