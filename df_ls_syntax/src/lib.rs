use tree_sitter::{Language, Parser};

extern "C" {
    fn tree_sitter_df_raw() -> Language;
}

pub fn parse() {
    let mut parser = Parser::new();

    let language = unsafe { tree_sitter_df_raw() };
    parser.set_language(language).unwrap();

    let source_code = "creature_domestic

    [OBJECT:CREATURE]

    [CREATURE:DOG]
        [CASTE:FEMALE]
            [FEMALE]
        [CASTE:MALE]
            [MALE]
            [DESCRIPTION:A male dog; has a different description to normal dogs.]
        [SELECT_CASTE:ALL]
            [NATURAL]
    ";
    let tree = parser.parse(source_code, None).unwrap();
    let root_node = tree.root_node();

    println!(
        "Tree: \n{}\n---",
        root_node.to_sexp().replace("(comment)", "\n(comment)")
    );

    assert_eq!(root_node.kind(), "raw_file");
    #[allow(unused_mut)]
    let mut subnode = root_node.child(2).unwrap();
    assert_eq!(subnode.kind(), "objects");
    // subnode = subnode.child(2).unwrap();

    assert_eq!(root_node.has_error(), false);
    let mut tree_cursor = root_node.walk();
    let mut creature_count = 0;
    for node in subnode.children_by_field_name("id", &mut tree_cursor) {
        creature_count += 1;
        println!("Creature: {}", node.kind());
    }
    println!("There are {} creatures", creature_count);
    // assert_eq!(root_node.end_position().column, 0);
}
