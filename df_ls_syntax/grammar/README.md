# DF Raw Grammar

This files in this folder are used to create the tree-sitter grammar.
Some of these files are generated.

You can use the `./create_grammar.sh` script.
Or use cargo to build/run the code, it will generate them.

To edit/change the grammar edit the [`grammar.js`](./grammar.js) file.
