const g = require("./general");
module.exports = {
	//-------------------------
	//---DESCRIPTOR_PATTERN----
	//-------------------------
	object_descriptor_pattern: $ =>
		prec.right(seq(
			// Ex: [OBJECT:DESCRIPTOR_PATTERN] ...
			g.start_object,
			'DESCRIPTOR_PATTERN',
			g.end_object,
			repeat(choice($.comment, $.color_pattern)),
		)),

	color_pattern: $ =>
		prec.right(seq(
			// Ex: [COLOR_PATTERN:IRIS_EYE_AMBER] ...
			g.token_open_bracket,
			'COLOR_PATTERN',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),
};