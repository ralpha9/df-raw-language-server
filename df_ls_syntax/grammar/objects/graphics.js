const g = require("./general");
module.exports = {
	//-------------------------
	//---------GRAPHICS----------
	//-------------------------
	object_graphics: $ =>
		prec.right(seq(
			// Ex: [OBJECT:GRAPHICS] ...
			g.start_object,
			'GRAPHICS',
			g.end_object,
			repeat(choice($.comment, $.tile_page, $.creature_graphics)),
		)),

	tile_page: $ =>
		prec.right(seq(
			// Ex: [TILE_PAGE:DWARVES] ...
			g.token_open_bracket,
			'TILE_PAGE',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),

	creature_graphics: $ =>
		prec.right(seq(
			// Ex: [CREATURE_GRAPHICS:DWARF] ...
			g.token_open_bracket,
			'CREATURE_GRAPHICS',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),
};