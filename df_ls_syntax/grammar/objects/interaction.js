const g = require("./general");
module.exports = {
	//-------------------------
	//-------INTERACTION-------
	//-------------------------
	object_interaction: $ =>
		prec.right(seq(
			// Ex: [OBJECT:INTERACTION] ...
			g.start_object,
			'INTERACTION',
			g.end_object,
			repeat(choice($.comment, $.interaction)),
		)),

	interaction: $ =>
		prec.right(seq(
			// Ex: [TODO] ...
			g.token_open_bracket,
			'INTERACTION',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),
};