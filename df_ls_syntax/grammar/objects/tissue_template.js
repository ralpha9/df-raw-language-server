const g = require("./general");
module.exports = {
	//-------------------------
	//-----TISSUE_TEMPLATE-----
	//-------------------------
	object_tissue_template: $ =>
		prec.right(seq(
			// Ex: [OBJECT:TISSUE_TEMPLATE] ...
			g.start_object,
			'TISSUE_TEMPLATE',
			g.end_object,
			repeat(choice($.comment, $.tissue_template)),
		)),

	tissue_template: $ =>
		prec.left(seq(
			// Ex: [TISSUE_TEMPLATE:SKIN_TEMPLATE] ...
			g.token_open_bracket,
			'TISSUE_TEMPLATE',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),
};