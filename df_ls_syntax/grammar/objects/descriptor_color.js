const g = require("./general");
module.exports = {
	//-------------------------
	//----DESCRIPTOR_COLOR-----
	//-------------------------
	object_descriptor_color: $ =>
		prec.right(seq(
			// Ex: [OBJECT:DESCRIPTOR_COLOR] ...
			g.start_object,
			'DESCRIPTOR_COLOR',
			g.end_object,
			repeat(choice($.comment, $.color)),
		)),

	color: $ =>
		prec.right(seq(
			// Ex: [COLOR:AMBER] ...
			g.token_open_bracket,
			'COLOR',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),
};