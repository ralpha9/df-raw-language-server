const g = require("./general");
module.exports = {
	//-------------------------
	//--------CREATURE---------
	//-------------------------
	object_creature: $ =>
		seq(
			// Ex: [OBJECT:CREATURE] ...
			g.start_object,
			'CREATURE',
			g.end_object,
			repeat(choice($.comment, $.creature)),
		),

	creature: $ =>
		seq(
			// Ex: [CREATURE:TOAD] ...
			g.token_open_bracket,
			'CREATURE',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice(
				$.comment,
				$.caste_token,
				$.creature_only_token,
			)),
		),

	caste_token: $ => // These can be part of either the main creature or a specific caste
		choice(
			$.adopts_owner,
			$.common_domestic,
			$.natural,
			$.c_description,
			$.petvalue,
			$.c_body,
			$.female,
			$.male,
		),

	creature_only_token: $ =>
		choice(
			$.c_name,
			$.prefstring,
			$.c_color,
			$.creature_tile,
			$.caste,
			$.select_caste
		),

	caste: $ =>
		seq(
			g.token_open_bracket,
			'CASTE',
			g.token_separator,
			$.token_value_reference,
			g.token_close_bracket,
			repeat(choice($.caste_token, $.comment)),
		),

	select_caste: $ =>
		seq(
			g.token_open_bracket,
			'SELECT_CASTE',
			g.token_separator,
			$.token_value_reference,
			g.token_close_bracket,
			repeat(choice($.caste_token, $.creature_only_token, $.comment)),
		),

	adopts_owner: $ => prec.right(seq(g.token_open_bracket, 'ADOPTS_OWNER', g.token_close_bracket,)),
	common_domestic: $ => prec.right(seq(g.token_open_bracket, 'COMMON_DOMESTIC', g.token_close_bracket,)),
	natural: $ => prec.right(seq(g.token_open_bracket, 'NATURAL', g.token_close_bracket,)),
	female: $ => prec.right(seq(g.token_open_bracket, 'FEMALE', g.token_close_bracket,)),
	male: $ => prec.right(seq(g.token_open_bracket, 'MALE', g.token_close_bracket,)),

	c_description: $ =>
		prec.right(seq(
			// Ex: [DESCRIPTION:Some string here]
			g.token_open_bracket,
			'DESCRIPTION',
			g.token_separator,
			field('description', $.token_value_string),
			g.token_close_bracket,
		)),

	c_name: $ =>
		prec.right(seq(g.token_open_bracket,
			'NAME',
			g.token_separator,
			$.token_value_string,
			g.token_separator,
			$.token_value_string,
			g.token_separator,
			$.token_value_string,
			g.token_close_bracket,
		)),

	prefstring: $ =>
		prec.right(seq(g.token_open_bracket,
			'PREFSTRING',
			g.token_separator,
			$.token_value_string,
			g.token_close_bracket,
		)),

	petvalue: $ =>
		prec.right(seq(g.token_open_bracket,
			'PETVALUE',
			g.token_separator,
			$.token_value_integer,
			g.token_close_bracket,
		)),

	c_color: $ =>
		prec.right(seq(
			g.token_open_bracket,
			'COLOR',
			g.token_separator,
			$.token_value_integer,
			g.token_separator,
			$.token_value_integer,
			g.token_separator,
			$.token_value_integer,
			g.token_close_bracket,
		)),

	creature_tile: $ =>
		prec.right(seq(g.token_open_bracket,
			'CREATURE_TILE', g.token_separator, $.token_value_character,
			g.token_close_bracket,
		)),

	c_body: $ =>
		prec.right(seq(g.token_open_bracket,
			'BODY',
			repeat(seq(
				g.token_separator,
				$.token_value_reference // later this should be more specific, restricted to body parts defined somewhere accessible
			)),
			g.token_close_bracket,
		)),
};
