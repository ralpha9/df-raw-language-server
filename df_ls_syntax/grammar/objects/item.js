const g = require("./general");
module.exports = {
	//-------------------------
	//----------ITEM-----------
	//-------------------------
	object_item: $ =>
		prec.right(seq(
			// Ex: [OBJECT:ITEM] ...
			g.start_object,
			'ITEM',
			g.end_object,
			repeat(choice(
				$.comment,
				$.item_ammo,
				$.item_armor,
				$.item_food,
				$.item_groves,
				$.item_helm,
				$.item_instrument,
				$.item_pants,
				$.item_shield,
				$.item_shoes,
				$.item_siegeammo,
				$.item_tool,
				$.item_toy,
				$.item_trapcomp,
				$.item_weapon,
			)),
		)),

	item_ammo: $ =>
		prec.right(seq(
			// Ex: [ITEM_AMMO:ITEM_AMMO_BOLTS] ...
			g.token_open_bracket,
			'ITEM_AMMO',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),

	item_armor: $ =>
		prec.right(seq(
			// Ex: [ITEM_ARMOR:ITEM_ARMOR_BREASTPLATE] ...
			g.token_open_bracket,
			'ITEM_ARMOR',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),

	item_food: $ =>
		prec.right(seq(
			// Ex: [ITEM_FOOD:ITEM_FOOD_BISCUITS] ...
			g.token_open_bracket,
			'ITEM_FOOD',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),

	item_groves: $ =>
		prec.right(seq(
			// Ex: [ITEM_GLOVES:ITEM_GLOVES_GAUNTLETS] ...
			g.token_open_bracket,
			'ITEM_GLOVES',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),

	item_helm: $ =>
		prec.right(seq(
			// Ex: [ITEM_HELM:ITEM_HELM_HELM] ...
			g.token_open_bracket,
			'ITEM_HELM',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),

	item_instrument: $ =>
		prec.right(seq(
			// Ex: [ITEM_INSTRUMENT:EXAMPLE DRUM] ...
			g.token_open_bracket,
			'ITEM_INSTRUMENT',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),

	item_pants: $ =>
		prec.right(seq(
			// Ex: [ITEM_PANTS:ITEM_PANTS_PANTS] ...
			g.token_open_bracket,
			'ITEM_PANTS',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),

	item_shield: $ =>
		prec.right(seq(
			// Ex: [ITEM_SHIELD:ITEM_SHIELD_SHIELD] ...
			g.token_open_bracket,
			'ITEM_SHIELD',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),

	item_shoes: $ =>
		prec.right(seq(
			// Ex: [ITEM_SHOES:ITEM_SHOES_SHOES] ...
			g.token_open_bracket,
			'ITEM_SHOES',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),

	item_siegeammo: $ =>
		prec.right(seq(
			// Ex: [ITEM_SIEGEAMMO:ITEM_SIEGEAMMO_BALLISTA] ...
			g.token_open_bracket,
			'ITEM_SIEGEAMMO',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),

	item_tool: $ =>
		prec.right(seq(
			// Ex: [ITEM_TOOL:ITEM_TOOL_CAULDRON] ...
			g.token_open_bracket,
			'ITEM_TOOL',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),

	item_toy: $ =>
		prec.right(seq(
			// Ex: [ITEM_TOY:ITEM_TOY_PUZZLEBOX] ...
			g.token_open_bracket,
			'ITEM_TOY',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),

	item_trapcomp: $ =>
		prec.right(seq(
			// Ex: [ITEM_TRAPCOMP:ITEM_TRAPCOMP_GIANTAXEBLADE] ...
			g.token_open_bracket,
			'ITEM_TRAPCOMP',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),

	item_weapon: $ =>
		prec.right(seq(
			// Ex: [ITEM_WEAPON:ITEM_WEAPON_WHIP] ...
			g.token_open_bracket,
			'ITEM_WEAPON',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),
};