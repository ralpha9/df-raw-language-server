const g = require("./general");
module.exports = {
	//-------------------------
	//---CREATURE_VARIATION----
	//-------------------------
	object_creature_variation: $ =>
		prec.right(seq(
			// Ex: [OBJECT:CREATURE_VARIATION] ...
			g.start_object,
			'CREATURE_VARIATION',
			g.end_object,
			repeat(choice($.comment, $.creature_variation)),
		)),

	creature_variation: $ =>
		prec.left(seq(
			// Ex: [CREATURE_VARIATION:ANIMAL_PERSON] ...
			g.token_open_bracket,
			'CREATURE_VARIATION',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),
};