const g = require("./general");
module.exports = {
	//-------------------------
	//--------LANGUAGE---------
	//-------------------------
	object_language: $ =>
		prec.right(seq(
			// Ex: [OBJECT:LANGUAGE] ...
			g.start_object,
			'LANGUAGE',
			g.end_object,
			repeat(choice(
				$.comment,
				$.translation,
				$.symbol,
				$.word,
			)),
		)),

	translation: $ =>
		prec.left(seq(
			// Ex: [TRANSLATION:DWARF] ...
			g.token_open_bracket,
			'TRANSLATION',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),

	symbol: $ =>
		prec.left(seq(
			// Ex: [SYMBOL:FLOWERY] ...
			g.token_open_bracket,
			'SYMBOL',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),

	word: $ =>
		prec.left(seq(
			// Ex: [WORD:ABBEY] ...
			g.token_open_bracket,
			'WORD',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),
};