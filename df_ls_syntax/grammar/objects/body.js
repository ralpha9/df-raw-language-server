const g = require("./general");
module.exports = {
	//-------------------------
	//----------BODY-----------
	//-------------------------
	object_body: $ =>
		prec.right(seq(
			// Ex: [OBJECT:BODY] ...
			g.start_object,
			'BODY',
			g.end_object,
			repeat(choice($.comment, $.body, $.bodygloss)),
		)),

	body: $ =>
		prec.right(seq(
			// Ex: [BODY:BASIC_1PARTBODY] ...
			g.token_open_bracket,
			'BODY',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),

	bodygloss: $ =>
		prec.right(seq(
			// Ex: [BODYGLOSS:PAW:foot:paw:feet:paws]
			g.token_open_bracket,
			'BODYGLOSS',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_separator,
			$.token_value_string,
			g.token_separator,
			$.token_value_string,
			g.token_separator,
			$.token_value_string,
			g.token_separator,
			$.token_value_string,
			g.token_close_bracket,
		)),
};