const g = require("./general");
module.exports = {
	//-------------------------
	//----------PLANT----------
	//-------------------------
	object_plant: $ =>
		prec.right(seq(
			// Ex: [OBJECT:PLANT] ...
			g.start_object,
			'PLANT',
			g.end_object,
			repeat(choice($.comment, $.plant)),
		)),

	plant: $ =>
		prec.left(seq(
			// Ex: [PLANT:MUSHROOM_HELMET_PLUMP] ...
			g.token_open_bracket,
			'PLANT',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),
};