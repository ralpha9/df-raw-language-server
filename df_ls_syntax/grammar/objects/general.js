// These have to exist outside the export for start_object and end_object to work
const token_open_bracket = '['
const token_close_bracket = ']'
const token_separator = ':'

module.exports = {
	// These 3 don't appear to be used anywhere but grammar.js yet
	newline: /\r?\n/,
	whitespace: /\s+/,
	line_space: /[ \t]+/,

	token_open_bracket: '[',
	token_close_bracket: ']',
	token_separator: ':',

	start_object: token_open_bracket + 'OBJECT' + token_separator,
	end_object: token_close_bracket,
};