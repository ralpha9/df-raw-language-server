const g = require("./general");
module.exports = {
	//-------------------------
	//---------ENTITY----------
	//-------------------------
	object_entity: $ =>
		prec.right(seq(
			// Ex: [OBJECT:ENTITY] ...
			g.start_object,
			'ENTITY',
			g.end_object,
			repeat(choice($.comment, $.entity)),
		)),

	entity: $ =>
		prec.right(seq(
			// Ex: [ENTITY:MOUNTAIN] ...
			g.token_open_bracket,
			'ENTITY',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),
};