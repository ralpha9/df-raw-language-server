const g = require("./general");
module.exports = {
	//-------------------------
	//--------REACTION---------
	//-------------------------
	object_reaction: $ =>
		prec.right(seq(
			// Ex: [OBJECT:REACTION] ...
			g.start_object,
			'REACTION',
			g.end_object,
			repeat(choice($.comment, $.reaction)),
		)),

	reaction: $ =>
		prec.left(seq(
			// Ex: [REACTION:TAN_A_HIDE] ...
			g.token_open_bracket,
			'REACTION',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),
};