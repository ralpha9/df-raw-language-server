const g = require("./general");
module.exports = {
	//-------------------------
	//----DESCRIPTOR_SHAPE-----
	//-------------------------
	object_descriptor_shape: $ =>
		prec.right(seq(
			// Ex: [OBJECT:DESCRIPTOR_SHAPE] ...
			g.start_object,
			'DESCRIPTOR_SHAPE',
			g.end_object,
			repeat(choice($.comment, $.shape)),
		)),

	shape: $ =>
		prec.right(seq(
			// Ex: [SHAPE:STAR] ...
			g.token_open_bracket,
			'SHAPE',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),
};