const g = require("./general");
module.exports = {
	//-------------------------
	//--------INORGANIC--------
	//-------------------------
	object_inorganic: $ =>
		prec.right(seq(
			// Ex: [OBJECT:INORGANIC] ...
			g.start_object,
			'INORGANIC',
			g.end_object,
			repeat(choice($.comment, $.inorganic)),
		)),

	inorganic: $ =>
		prec.right(seq(
			// Ex: [INORGANIC:IRON] ...
			g.token_open_bracket,
			'INORGANIC',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),
};