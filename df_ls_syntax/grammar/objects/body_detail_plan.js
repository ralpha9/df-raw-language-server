const g = require("./general");
module.exports = {
	//-------------------------
	//----BODY_DETAIL_PLAN-----
	//-------------------------
	object_body_detail_plan: $ =>
		prec.right(seq(
			// Ex: [OBJECT:BODY_DETAIL_PLAN] ...
			g.start_object,
			'BODY_DETAIL_PLAN',
			g.end_object,
			repeat(choice($.comment, $.body_detail_plan)),
		)),

	body_detail_plan: $ =>
		prec.right(seq(
			// Ex: [BODY_DETAIL_PLAN:STANDARD_MATERIALS] ...
			g.token_open_bracket,
			'BODY_DETAIL_PLAN',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),
};