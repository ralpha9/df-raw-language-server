const g = require("./general");
module.exports = {
	//-------------------------
	//----MATERIAL_TEMPLATE----
	//-------------------------
	object_material_template: $ =>
		prec.right(seq(
			// Ex: [OBJECT:MATERIAL_TEMPLATE] ...
			g.start_object,
			'MATERIAL_TEMPLATE',
			g.end_object,
			repeat(choice($.comment, $.material_template)),
		)),

	material_template: $ =>
		prec.left(seq(
			// Ex: [MATERIAL_TEMPLATE:STONE_TEMPLATE] ...
			g.token_open_bracket,
			'MATERIAL_TEMPLATE',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),
};