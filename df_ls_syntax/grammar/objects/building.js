const g = require("./general");
module.exports = {
	//-------------------------
	//--------BUILDING---------
	//-------------------------
	object_building: $ =>
		prec.right(seq(
			// Ex: [OBJECT:BUILDING] ...
			g.start_object,
			'BUILDING',
			g.end_object,
			repeat(choice($.comment, $.building_workshop)),
		)),

	building_workshop: $ =>
		prec.right(seq(
			// Ex: [BUILDING_WORKSHOP:SOAP_MAKER] ...
			g.token_open_bracket,
			'BUILDING_WORKSHOP',
			g.token_separator,
			field('id', $.token_value_reference),
			g.token_close_bracket,
			repeat(choice($.comment)),
		)),
};