const newline = /\r?\n/;
const whitespace = /\s+/;
const line_space = /[ \t]+/;
const comment = /[^\[\]]+/;

const token_value_integer = /-?[0-9]+/;
const token_value_reference = /[0-9]*[A-Z][A-Z_0-9]*/;
const token_value_enum = /[0-9]*[A-Z][A-Z_0-9]*/;
const token_value_character = /('.')|([0-9]{1,3})/;
const token_value_string = /[^\[\]:\n\r]+/;

const g = require("./objects/general");

const body_detail_plan_rules = require("./objects/body_detail_plan");
const body_rules = require("./objects/body");
const building_rules = require("./objects/building");
const creature_variation_rules = require("./objects/creature_variation");
const creature_rules = require("./objects/creature");
const descriptor_color_rules = require("./objects/descriptor_color");
const descriptor_pattern_rules = require("./objects/descriptor_pattern");
const descriptor_shape_rules = require("./objects/descriptor_shape");
const entity_rules = require("./objects/entity");
const graphics_rules = require("./objects/graphics");
const inorganic_rules = require("./objects/inorganic");
const interaction_rules = require("./objects/interaction");
const item_rules = require("./objects/item");
const language_rules = require("./objects/language");
const material_template_rules = require("./objects/material_template");
const plant_rules = require("./objects/plant");
const reaction_rules = require("./objects/reaction");
const tissue_template_rules = require("./objects/tissue_template");

const general_rules = {
    raw_file: $ =>
        seq(
            // Header must be present
            optional(line_space), $.header, newline,
            optional($.comment),
            // Comments or `[OBJECT:...]` structures
            // repeat(choice($.comment, $.objects)),
            repeat(choice($.objects)),
            // optional($.comment),
        ),

    header: $ => /\w[^\n]*/,

    comment: $ => //comment,
        token(comment),

    token_value_integer: $ => token(token_value_integer),
    token_value_reference: $ => token(token_value_reference),
    token_value_enum: $ => token(token_value_enum),
    token_value_character: $ => token(token_value_character),
    token_value_string: $ => token(token_value_string),

    objects: $ =>
        choice(
            $.object_body,
            $.object_body_detail_plan,
            $.object_building,
            $.object_creature,
            $.object_creature_variation,
            $.object_descriptor_color,
            $.object_descriptor_pattern,
            $.object_descriptor_shape,
            $.object_entity,
            $.object_graphics,
            $.object_interaction,
            $.object_inorganic,
            $.object_item,
            $.object_language,
            $.object_material_template,
            $.object_plant,
            $.object_reaction,
            $.object_tissue_template,
        ),
    };
const grammar_rules = Object.assign({}, general_rules, 
        body_detail_plan_rules,
        body_rules,
        building_rules,
        creature_variation_rules,
        creature_rules, 
        descriptor_color_rules,
        descriptor_pattern_rules,
        descriptor_shape_rules,
        entity_rules,
        graphics_rules,
        inorganic_rules,
        interaction_rules,
        item_rules,
        language_rules,
        material_template_rules,
        plant_rules,
        reaction_rules,
        tissue_template_rules
    );

// Different function: https://tree-sitter.github.io/tree-sitter/creating-parsers#the-grammar-dsl
// Example: https://tree-sitter.github.io/tree-sitter/creating-parsers#the-first-few-rules

module.exports = grammar({
    name: 'df_raw',

    conflicts: $ => [
        // There will be a lot of conflict as DF RAW don't have closing tags
        [$.creature, $.creature],
        [$.caste, $.caste],
        [$.select_caste, $.select_caste],
    ],

    rules: grammar_rules
});
