#!/bin/sh

# Download new release from: https://github.com/tree-sitter/tree-sitter/releases
# Current version 0.17.1 (2020/10/10)

./tree-sitter-bin/tree-sitter_linux generate
#./tree-sitter-bin/tree-sitter_macos generate
#./tree-sitter-bin/tree-sitter_windows.exe generate
