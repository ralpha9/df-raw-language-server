use std::process::Command;

#[test]
fn test_grammar() {
    // Test grammar using Tree-Sitter

    // Get tree sitter executable depending on platform
    let mut tree_sitter = if cfg!(target_os = "linux") {
        Command::new("./tree-sitter-bin/tree-sitter_linux")
    } else if cfg!(target_os = "windows") {
        Command::new("./tree-sitter-bin/tree-sitter_windows.exe")
    } else if cfg!(target_os = "macos") {
        Command::new("./tree-sitter-bin/tree-sitter_macos")
    } else {
        panic!("This is only supported for Linux, Windows and macOS.");
    };

    println!("Test Grammar");
    let output = tree_sitter
        .args(&["test"])
        .current_dir("./grammar/")
        .output()
        .expect("failed to execute testing of grammar");

    println!("status: {}", output.status);
    println!("stdout: {}", String::from_utf8_lossy(&output.stdout));
    println!("stderr: {}", String::from_utf8_lossy(&output.stderr));
    assert!(output.status.success());
}
