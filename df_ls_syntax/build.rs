use std::path::PathBuf;
use std::process::Command;
use std::io::Write;

fn main() {
    // Generate grammar using Tree-Sitter
    let mut generate_grammar = if cfg!(target_os = "linux") {
        Command::new("./tree-sitter-bin/tree-sitter_linux")
    } else if cfg!(target_os = "windows") {
        Command::new("./tree-sitter-bin/tree-sitter_windows.exe")
    } else if cfg!(target_os = "macos") {
        Command::new("./tree-sitter-bin/tree-sitter_macos")
    } else {
        panic!("This is only supported for Linux, Windows and macOS.");
    };

    println!("Generate Grammar");
    let output = generate_grammar
        .args(&["generate"])
        .current_dir("./grammar/")
        .output()
        .expect("failed to execute generation of grammar");

    std::io::stdout().write_all(&output.stdout).unwrap();
    std::io::stderr().write_all(&output.stderr).unwrap();
    assert!(output.status.success());
    println!("Generate Grammar: Done");

    let dir = PathBuf::from("grammar/src");
    // Take the generated grammar and build them into the binary
    cc::Build::new()
        .include(&dir)
        .file(dir.join("parser.c"))
        // .file(dir.join("scanner.c"))
        .compile("tree-sitter-df-raw");
}
