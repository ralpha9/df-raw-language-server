# Dwarf Fortress RAW language server

This repo is for a language server for [Dwarf Fortress RAW files](https://dwarffortresswiki.org/index.php/Raw_file).
These files can be used to modify (mod) the game.

This repo is work in progress so it does not work yet.

## Syntax

If you want to take a look at the syntax this extension is based on look here:
https://gitlab.com/df-modding-tools/df-raw-syntax

## Contribute

If you want to contribute, join our [Discord](https://discord.gg/6eKf5ZY).

## License

This project is licensed under the MIT license.

All contributions to this project will be similarly licensed.
